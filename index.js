const express = require("express");
const app = express();
const port = 8000;

const path = require("path");

app.get("/", (req, res) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/view/example-call.html"));
});

app.listen(port, () => {
    console.log("App listening on port: ", port);
});